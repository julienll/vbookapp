import React from 'react';
import { Platform,  Image } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import FavsScreen from '../screens/FavsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import BasketScreen from '../screens/BasketScreen';
import SettingsScreen from '../screens/SettingsScreen';
import DetailsScreen from '../screens/DetailsScreen';
import SceneScreen from '../screens/SceneScreen';
import PaymentScreen from '../screens/PaymentScreen';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: ' ',
  tabBarIcon: ({ focused }) => {
    const image = focused
        ? require('../assets/images/home.png')
        : require('../assets/images/greyHome.png')
        return (
            <Image
                source={image}
                style={{ height: 40, width: 30, marginTop: 12 }}
                resizeMode='contain'
            />
        )
  },
};

HomeStack.path = '';

const FavsStack = createStackNavigator(
  {
    Favs: FavsScreen,
    Details: DetailsScreen,
  },
  config
);

FavsStack.navigationOptions = {
  tabBarLabel: ' ',
  tabBarIcon: ({ focused }) => {
    const image = focused
        ? require('../assets/images/favs.png')
        : require('../assets/images/greyFavs.png')
        return (
            <Image
                source={image}
                style={{ height: 35, width: 25, marginTop: 12 }}
                resizeMode='contain'
            />
        )
  },
};

FavsStack.path = '';

const ProfileStack = createStackNavigator(
  {
    Profile: ProfileScreen,
    Details: DetailsScreen,
    Scene: SceneScreen,
  },
  config
);

ProfileStack.navigationOptions = {
  tabBarLabel: ' ',
  tabBarIcon: ({ focused }) => {
    const image = focused
        ? require('../assets/images/profile.png')
        : require('../assets/images/greyProfile.png')
        return (
            <Image
                source={image}
                style={{ height: 45, width: 35, marginTop: 12 }}
                resizeMode='contain'
            />
        )
  },
};

ProfileStack.path = '';

const BasketStack = createStackNavigator(
  {
    Basket: BasketScreen,
    Payment: PaymentScreen,
    Details: DetailsScreen,
  },
  config
);

BasketStack.navigationOptions = {
  tabBarLabel: ' ',
  tabBarIcon: ({ focused }) => {
    const image = focused
        ? require('../assets/images/basket.png')
        : require('../assets/images/greyBasket.png')
        return (
            <Image
                source={image}
                style={{ height: 40, width: 30, marginTop: 12 }}
                resizeMode='contain'
            />
        )
  },
};

BasketStack.path = '';

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
  },
  config
);

SettingsStack.navigationOptions = {
  tabBarLabel: ' ',
  tabBarIcon: ({ focused }) => {
    const image = focused
        ? require('../assets/images/settings.png')
        : require('../assets/images/greySettings.png')
        return (
            <Image
                source={image}
                style={{ height: 40, width: 30, marginTop: 12 }}
                resizeMode='contain'
            />
        )
  },
};

SettingsStack.path = '';

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  FavsStack,
  ProfileStack,
  BasketStack,
  SettingsStack,
});

tabNavigator.path = '';

export default tabNavigator;
