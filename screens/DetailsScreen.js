import React from 'react';
import { View, Button, AsyncStorage, Image,
  Text, Dimensions, ScrollView, Platform } from 'react-native';
import blankImg from '../assets/images/blank.png';
import axios from 'axios';

const decode = require('jwt-decode');
const moment = require('moment');
const ACCESS_TOKEN = 'access_token';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class DetailsScreen extends React.Component {
  constructor(props) {
    super(props);

    const { navigation } = this.props;
    const vbook = navigation.getParam('vbook');

    this.state = {
      vbook: vbook,
      user_id: null,
      auth: null,
    };
  }

  static navigationOptions = {
    title: 'DETAILS',
  };

  /****************** CLASSIC FUNCTIONS *****************/

  /****************** ASYNC FUNCTIONS *****************/

  async addToBasket() {
    await axios.post('https://vbookcreator-dev.herokuapp.com/basket/', {
      basket: [this.state.vbook],
      user: this.user_id,
    }, { headers: { Authorization: this.auth } })
    .then(res => {
      this.props.navigation.navigate('Basket');
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async getToken() {
    try {
      let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
      if (!accessToken) {
        console.log("Token not set");
      } else {
        var userId = decode(accessToken);
        this.user_id = userId.userId;
        return accessToken;
      }
    } catch(error) {
      console.log("Home-getToken : ", error);
    }
  }

  async componentDidMount() {
    const token = await this.getToken();
    const AuthStr = 'Bearer '.concat(token);
    this.auth = AuthStr;
  }

  /****************** RENDER FUNCTIONS *****************/

  renderImg = () => {
    if (this.state.vbook.img == "blank") {
      return (
        <Image source={ blankImg }
        style={{ height: 120, width: 140 }} />
      );
    }
    return (
      <Image source={{ uri: `${this.state.vbook.img}`}}
        style={{ height: 120, width: 140 }}/>
    );
  }

  checkButton = (padd, background, text, title, press) => {
    if (Platform.OS == "ios") {
    return (
      <View style={{backgroundColor: background, margin: 20, borderRadius: 6, padding: padd }}>
      <Button
        title={title}
        onPress={press}
        color={text}
      />
      </View>)
    } else {
      if (padd == 0) {
        return (
          <View style={{borderRadius: 6, padding: padd }}>
          <Button
          title={title}
          onPress={press}
          color={text}
          />
          </View>)
      } else {
      return (
        <View style={{borderRadius: 6, padding: padd }}>
        <Button
        title={title}
        onPress={press}
        color={background}
        />
        </View>)}
    }
  }

  render() {
    return (
      <View>
        <View style={{flexDirection: 'row'}}>
        <View style={{ shadowColor: "#000000", shadowOffset: {width: 0, height: 2}, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5, marginBottom: 10 }}>
          <View style={{ width: 140, flexDirection: 'row', backgroundColor: 'white', margin: 20, borderRadius: 6, overflow: 'hidden' }}>
          {this.renderImg()}
          </View>
          </View>
          <View style={{marginTop: 20}}>
            <Text style={{fontWeight: 'bold', marginBottom: 10}}> {this.state.vbook.title} </Text>
            <Text> Auteur </Text>
            <Text> 4€ </Text>
          </View>
        </View>
        {this.checkButton(10, "#00ace0", "#ffffff", "AJOUTER AU PANIER", () => this.addToBasket())}
        <ScrollView>
        <Text style={{fontWeight: 'bold', margin: 20}}> À propos </Text>
        <Text style={{marginLeft: 20, marginRight: 20}}> {this.state.vbook.about} </Text>
        </ScrollView>
      </View>
    );
  }
}

export default DetailsScreen;
