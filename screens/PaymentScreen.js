import React from 'react';
import { View, Button, Alert, AsyncStorage, Platform } from 'react-native';
import { CreditCardInput } from "react-native-credit-card-input";
import axios from 'axios';

const decode = require('jwt-decode');
const moment = require('moment');
const ACCESS_TOKEN = 'access_token';

class PaymentScreen extends React.Component {

  static navigationOptions = {
      title: 'Paiement',
}
      constructor(props) {
        super(props);

        const { navigation } = this.props;
        const basket = navigation.getParam('basket');

        this.state = {
          input: null,
          basket: basket,
          userId: null,
          auth: null,
        };
      }

  onChangeCardInput(text) {
    this.setState({ input: text })
  }

  async postVBook(vbook) {
    await axios.post('https://vbookcreator-dev.herokuapp.com/purchases/', {
      vbook: vbook.vbook_id,
      user: this.userId,
    }, { headers: { Authorization: this.auth } })
    .then(res => {
      this.deleteBasket();
      this.props.navigation.navigate('Profile');
    })
    .catch(function (error) {
      console.log(error);
    });
  }


  async deleteBasket() {
    await axios.delete('https://vbookcreator-dev.herokuapp.com/basket/?user=' + this.userId, { headers: { Authorization: this.auth } })
    .then(function (response) {
      console.log("delete basket");
    })
    .catch(function (e) {
      console.log("delete", e);
    });
  }

  buy() {
    if (this.state.input == null) {
      Alert.alert('Merci de remplir tous les champs', '',
      [{text: 'OK', onPress: () => console.log('OK Pressed')},],
      {cancelable: false})
    } else if (this.state.input.values.number != "" && this.state.input.values.expiry != "" && this.state.input.values.cvc != "") {
      console.log("ACHAT");
      this.state.basket.map((y, index) => {
        this.postVBook(y);
        if (index == this.state.basket.lenght - 1) {
          console.log("here");
          this.deleteBasket();
        }
      })
    } else {
      Alert.alert('Merci de remplir tous les champs', '',
      [{text: 'OK', onPress: () => console.log('OK Pressed')},],
      {cancelable: false})
    }
  }

  async getToken() {
      try {
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        if (!accessToken) {
          console.log("Token not set");
        } else {
          var userId = decode(accessToken);
          console.log('ID ', userId);
          return accessToken;
        }
      } catch (error) {
        console.log("Payment-getToken : ", error);
}
}

  async componentDidMount() {
    const token = await this.getToken();
      this.auth = 'Bearer '.concat(token);
      this.userId = decode(token).userId;
  }

  checkButton = (padd, background, text, title, press) => {
    if (Platform.OS == "ios") {
    return (
      <View style={{backgroundColor: background, margin: 20, borderRadius: 6, padding: padd }}>
      <Button
        title={title}
        onPress={press}
        color={text}
      />
      </View>)
    } else {
      if (padd == 0) {
        return (
          <View style={{borderRadius: 6, padding: padd }}>
          <Button
          title={title}
          onPress={press}
          color={text}
          />
          </View>)
      } else {
      return (
        <View style={{borderRadius: 6, padding: padd }}>
        <Button
        title={title}
        onPress={press}
        color={background}
        />
        </View>)}
    }
  }

  render() {
    return (
      <View style={{marginTop: 20}}>
        <CreditCardInput onChange={text => this.onChangeCardInput(text)}/>
        {this.checkButton(10, "#00ace0", "#ffffff", "VALIDER L'ACHAT", () => this.buy())}
      </View>
    );
  }
}

export default PaymentScreen;
