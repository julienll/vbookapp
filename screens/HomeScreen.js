import React from 'react';
import { Image, Platform, ScrollView, StyleSheet,
  Text, TouchableOpacity, View, Button,
  FlatList, AsyncStorage, Dimensions } from 'react-native';
import { Input } from 'react-native-elements';
import axios from 'axios';
import redHeart from '../assets/images/red-heart.png';
import emptyHeart from '../assets/images/empty-heart.png';
import blankImg from '../assets/images/blank.png';

const decode = require('jwt-decode');
const moment = require('moment');
const ACCESS_TOKEN = 'access_token';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: null,
      vbook: [],
      auth: null,
      user_id: null,
      data: [],
      favs: [],
      showHeart: false,
      saveVBook: []
    };
  }

/****************** CLASSIC FUNCTIONS *****************/

searchFilterFunction(text) {
    const newData = this.state.saveVBook.filter(function(item) {
      const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      vbook: newData,
      text: text,
    });
  }

  checkFav(vbook_id) {
    var i = 0;
    this.state.favs.map((y) => {
      if (vbook_id == y.vbook) {
        i = 1;
      }
    });
    if (i == 1) {
      this.showHeart = true;
    } else {
      this.showHeart = false
    }
  }

/****************** ASYNC FUNCTIONS *****************/

async onFavPressed(vbook) {
  if (!vbook.fav) {
    await axios.post('https://vbookcreator-dev.herokuapp.com/favourites/', {
      vbook: vbook.vbook_id,
      user: this.user_id,
    }, { headers: { Authorization: this.auth } })
    .then(res => {
    })
    .catch(function (error) {
      console.log(error);
    });
  } else {
    this.state.favs.map((y) => {
      if (vbook.vbook_id == y.vbook) {
        axios.delete('https://vbookcreator-dev.herokuapp.com/favourites/' + y._id, { headers: { Authorization: this.auth } })
        .then(function (response) {
          console.log("delete fav");
        })
        .catch(function (e) {
          console.log(e);
        });
      }
    });
  }
  vbook.fav = !vbook.fav;
  this.setState({showHeart: !this.state.showHeart})
}

  async getFavs() {
    await axios.get('https://vbookcreator-dev.herokuapp.com/favourites?user=' + this.user_id, { headers: { Authorization: this.auth } })
    .then(res => {
      this.setState({
        favs: res.data.data,
      });
    })
    .catch((error) => {
      console.log('Home-getFavs : ' + error);
    });
  }

  async getVBookPicture(picture_id, title, about, vbook_id, vbook_date, author) {
    await axios.get('https://vbookcreator-dev.herokuapp.com/upload-image/' + picture_id, { headers: { Authorization: this.auth } })
    .then(res => {
      this.checkFav(vbook_id);
      this.setState(prevState  =>  ({
        vbook: [...prevState.vbook, {"img": res.data.uri, "title": title, "about": about, "vbook_id": vbook_id,
                                      "picture_id": picture_id, "createdAt": vbook_date, "fav": this.showHeart, author: author}],
        saveVBook: [...prevState.vbook, {"img": res.data.uri, "title": title, "about": about, "vbook_id": vbook_id,
                                          "picture_id": picture_id, "createdAt": vbook_date, "fav": this.showHeart, author: author}]
      }))
    })
    .catch((error) => {
      this.checkFav(vbook_id);
      this.setState(prevState  =>  ({
        vbook: [...prevState.vbook, {"img": "blank", "title": title, "about": about, "vbook_id": vbook_id,
                                      "picture_id": picture_id, "createdAt": vbook_date, "fav": this.showHeart, author: author}],
        saveVBook: [...prevState.vbook, {"img": "blank", "title": title, "about": about, "vbook_id": vbook_id,
                                        "picture_id": picture_id, "createdAt": vbook_date, "fav": this.showHeart, author: author}]

      }))
    })
    if (this.state.vbook.length == this.state.total) {
      var mostRecentDate = new Date(Math.max.apply(null, this.state.vbook.map( e => {
         return new Date(e.createdAt);
      })));
      for(var i = 0; i < this.state.vbook.length; i++) {
        var d = new Date(this.state.vbook[i]['createdAt']);
        if(d.getTime() === mostRecentDate.getTime()) {
            this.setState({
              titleRecent: this.state.vbook[i].title,
              aboutRecent: this.state.vbook[i].about,
              imgRecent: this.state.vbook[i].img,
              authorRecent: this.state.vbook[i].author
            });
        }
      }
    }

  }

  async getVBookData() {
    await axios.get('https://vbookcreator-dev.herokuapp.com/vbooks?$limit=25', { headers: { Authorization: this.auth } })
    .then(res => { this.setState({
    data: res.data.data,
    total: res.data.limit,
     titleRecent: '',
     aboutRecent: '',
     imgRecent: blankImg,
     author: '',
   })
})
    .catch((error) => {
      console.log("Home-getVBookData : ", error);
    })
  }

  async getToken() {
    try {
      let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
      if (!accessToken) {
        console.log("Token not set");
      } else {
        var userId = decode(accessToken);
        this.user_id = userId.userId;
        return accessToken;
      }
    } catch(error) {
      console.log("Home-getToken : ", error);
    }
  }

  async _asyncGet() {
    await this.getFavs();
    await this.getVBookData();
    this.state.data.map((i) => {
      this.getVBookPicture(i.picture, i.title, i.about, i._id, i.createdAt, i.author);
    });
  }

  async componentDidMount() {
    const token = await this.getToken();
    const AuthStr = 'Bearer '.concat(token);
    this.auth = AuthStr;
    this._asyncGet();

    this.didFocusListener = this.props.navigation.addListener(
    'didFocus',
    () => {
            console.log('did focus');
            this.setState({ vbook: []})
            this._asyncGet();
          },
  );
  }

/****************** RENDER FUNCTIONS *****************/

renderImgNew = () => {
  if (this.state.imgRecent == "blank") {
    return (
      <Image source={ blankImg }
      style={{ width: screenHeight / 6,  height: screenHeight / 6}} />
    );
  }
  return (
    <Image source={{ uri: `${this.state.imgRecent}`}}
      style={{ width: screenHeight / 6,  height: screenHeight / 6}}/>
  );
}

  renderNewVBook = () => {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {vbook: {"img": this.state.imgRecent, "about": this.state.aboutRecent, "title": this.state.titleRecent}})}>
        <View style={{borderWidth: 0.5, borderColor: '#d6d7da', width: screenWidth - 20, flexDirection: 'row', backgroundColor: 'white', margin: 10, borderRadius: 6, overflow: 'hidden' }}>
          {this.renderImgNew()}
          <View>
            <Text style={{ margin: 10,  fontWeight: 'bold' }}>
              {this.state.titleRecent}
            </Text>
            <Text style={{ width: screenWidth - (screenHeight / 6) - 40, marginLeft: 10}}>
            {this.state.aboutRecent}
              </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  renderHeart = (vbook) => {
    var imgSource = vbook.fav? redHeart : emptyHeart;
    return (
      <Image
      style={{width: 30, height: 40}}
        source={ imgSource }
        resizeMode="contain"
      />
    );
  }

  renderImg = (vbook) => {
    if (vbook.img == "blank") {
      return (
        <Image source={ blankImg }
        style={{ height: screenHeight / 8, width: (screenWidth / 2) - 10}} />
      );
    }
    return (
      <Image source={{ uri: `${vbook.img}`}}
        style={{ height: screenHeight / 8 }}/>
    );
  }

  renderVbookItem = (item) => {
    return (
        <View style={{backgroundColor: 'white', width: ((screenWidth / 2) - 15), marginLeft: 10, marginTop: 10,
                      borderRadius: 6, overflow: 'hidden', borderWidth: 0.5, borderColor: '#d6d7da', }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {vbook: item.item})}>
            {this.renderImg(item.item)}
          </TouchableOpacity>
          <Text style={{ marginLeft: 5, marginTop: 10, fontWeight: 'bold' }}> {item.item.title} </Text>
          <Text style={{ marginLeft: 5 }}> {item.item.author} </Text>
          <Text style={{ margin: 10 }}> 4€ </Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {vbook: item.item})}
              style={{ backgroundColor: "#00ace0", justifyContent: 'center', alignItems: 'center', width: ((screenWidth / 2) - 15) / 2 }}>
              <Image source={require('../assets/images/view.png')}
                style={{width: 30, height: 40}}
                resizeMode="contain"/>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onFavPressed(item.item)}
              style={{justifyContent: 'center', alignItems: 'center', width: ((screenWidth / 2) - 15) / 2}}>
              {this.renderHeart(item.item)}
            </TouchableOpacity>
          </View>
        </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>

        <View style={{ flexDirection: 'row', marginBottom: 20,}}>
          <View style={{ marginTop: 10, width: screenWidth - 50 }}>
            <Input
              placeholder="Rechercher un titre ..."
              onChangeText={text => this.searchFilterFunction(text)}
            />
          </View>
          <Image source={require('../assets/images/search.png')}
            style={{width: 30, height: 40, marginTop: 10}}
            resizeMode="contain"/>
        </View>

          <Text style={{ fontWeight: 'bold', color: "#00ace0", fontSize: 20, marginLeft: 10 }}>
            NOUVEAUTÉ
          </Text>
          {this.renderNewVBook()}

          <Text style={{ fontWeight: 'bold', color: "#00ace0", fontSize: 20, marginLeft: 10 }}>
            NOS VBOOK
          </Text>
          <FlatList
            data={this.state.vbook}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(item) => this.renderVbookItem(item)}
            numColumns={2}
            style={{ paddingBottom: 20 }}
          />

        </ScrollView>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  title: 'Accueil',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default HomeScreen;
