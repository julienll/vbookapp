import React from 'react';
import { ScrollView, StyleSheet, View, TouchableOpacity,
   Image, Dimensions, Text, Button,
   AsyncStorage, FlatList, Platform } from 'react-native';
import axios from 'axios';
import blankImg from '../assets/images/blank.png';

const decode = require('jwt-decode');
const moment = require('moment');
const ACCESS_TOKEN = 'access_token';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class BasketScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: null,
      auth: null,
      baskets: [],
      myBasket: [],
    }
  };

  /****************** CLASSIC FUNCTIONS *****************/

  /****************** ASYNC FUNCTIONS *****************/

  async addToBasket(item) {
    await axios.post('https://vbookcreator-dev.herokuapp.com/basket/', {
      basket: [item],
      user: this.user_id,
    }, { headers: { Authorization: this.auth } })
    .then(res => {
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async getBasket() {
    await axios.get('https://vbookcreator-dev.herokuapp.com/basket?user=' + this.user_id, { headers: { Authorization: this.auth } })
    .then(res => {
      this.setState({ baskets: res.data.data })
    })
    .catch((error) => {
      console.log('Basket-getBasket : ' + error);
    });

    this.state.baskets.map((i) => {
      this.setState(prevState => ({
        myBasket: [...prevState.myBasket, i.basket[0]]
      }))
    })
  }

  async deleteBasket(item) {
    this.state.myBasket.splice(item.index, 1);
    axios.delete('https://vbookcreator-dev.herokuapp.com/basket/?user=' + this.user_id, { headers: { Authorization: this.auth } })
    .then(function (response) {
      console.log("delete fav");
    })
    .catch(function (e) {
      console.log(e);
    });

    this.state.myBasket.map((i) => {
      this.addToBasket(i);
    })

    await this.getBasket();
  }

  async _asyncGet() {
    await this.getBasket();
  }

  async getToken() {
    try {
      let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
      if (!accessToken) {
        console.log("Token not set");
      } else {
        var userId = decode(accessToken);
        this.user_id = userId.userId;
        return accessToken;
      }
    } catch(error) {
      console.log("Basket-getToken : ", error);
    }
  }

  async componentDidMount() {
    const token = await this.getToken();
    const AuthStr = 'Bearer '.concat(token);
    this.auth = AuthStr;
    this._asyncGet();

    this.didFocusListener = this.props.navigation.addListener(
    'didFocus',
    () => {
            console.log('did focus');
            this.setState({ baskets: []});
            this.setState({ myBasket: []});
            this._asyncGet();
          },
  );
  }

  /***************  RENDER **********************/

  checkButton = (padd, background, text, title, press) => {
    if (Platform.OS == "ios") {
    return (
      <View style={{backgroundColor: background, margin: 20, borderRadius: 6, padding: padd }}>
      <Button
        title={title}
        onPress={press}
        color={text}
      />
      </View>)
    } else {
      if (padd == 0) {
        return (
          <View style={{borderRadius: 6, padding: padd }}>
          <Button
          title={title}
          onPress={press}
          color={text}
          />
          </View>)
      } else {
      return (
        <View style={{borderRadius: 6, padding: padd }}>
        <Button
        title={title}
        onPress={press}
        color={background}
        />
        </View>)}
    }
  }

  renderImg = (vbook) => {
    if (vbook.img == "blank") {
      return ( <Image source={ blankImg }
        style={{ width: screenHeight / 6,  height: screenHeight / 6}}/>
    )}
    return (
      <Image source={{ uri: `${vbook.img}`}}
      style={{ width: screenHeight / 6,  height: screenHeight / 6}}/>
  )
  }

  renderItem = (item) => {
    return (
      <View  style={{borderWidth: 0.5, borderColor: '#d6d7da', width: screenWidth - 20, flexDirection: 'row', backgroundColor: 'white', margin: 10, borderRadius: 6, overflow: 'hidden' }}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {vbook: item.item})}>
            {this.renderImg(item.item)}
            </TouchableOpacity>
        <View>
          <Text style={{ margin: 10, width: screenWidth - 240, fontWeight: 'bold'}}>
            {item.item.title}
          </Text>
          <Text style={{ marginLeft: 10, width: screenWidth - 230}}>
            4€
          </Text>
        </View>

        <View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {vbook: item.item})}
          style={{backgroundColor: "#00ace0", height: (screenHeight / 6) / 2, paddingTop: ((screenHeight / 6) / 2) / 4, paddingLeft: 10, paddingRight: 10}}>
            <Image source={require("../assets/images/view.png")}
            style={{height: 30, width: 30}}
            resizeMode="contain"/>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.deleteBasket(item)}>
            <Image source={require("../assets/images/garbage.png")}
            style={{height: 30, width: 30, height: (screenHeight / 6) / 2, marginLeft: 10}}
            resizeMode="contain"/>
          </TouchableOpacity>
        </View>
      </View>
  )
  }

  render() {
    return (
      <ScrollView>
      <FlatList
        data={this.state.myBasket}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(item) => this.renderItem(item)}
        numColumns={1}
        style={{ paddingBottom: 20 }}
      />
      {this.checkButton(10, "#00ace0", "#ffffff", "VALIDER LE PANIER", () => this.props.navigation.navigate('Payment', {basket: this.state.myBasket}))}
      </ScrollView>
    );
  }

}

BasketScreen.navigationOptions = {
  title: 'Panier',
};

export default BasketScreen;
