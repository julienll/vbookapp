import React from 'react';
import { View, Button, AsyncStorage, ScrollView,
  Image, Dimensions, Text, ActivityIndicator,
  StyleSheet, Alert, Platform, KeyboardAvoidingView } from 'react-native';

import { Input } from 'react-native-elements';
import Connection from '../utils/Connection';

class SignInScreen extends React.Component {
  static navigationOptions = {
    title: 'Connexion',
  };
  constructor(props) {
    super(props);

      this.state = {
        username: '',
        password: '',
        loader: null,
      };
    }

    checkButton = (padd, background, text, title, press) => {
      if (Platform.OS == "ios") {
      return (
        <View style={{backgroundColor: background, margin: 20, borderRadius: 6, padding: padd }}>
        <Button
          title={title}
          onPress={press}
          color={text}
        />
        </View>)
      } else {
        if (padd == 0) {
          return (
            <View style={{borderRadius: 6, padding: padd }}>
            <Button
            title={title}
            onPress={press}
            color={text}
            />
            </View>)
        } else {
        return (
          <View style={{borderRadius: 6, padding: padd }}>
          <Button
          title={title}
          onPress={press}
          color={background}
          />
          </View>)}
      }
    }

  render() {
    const screenWidth = Math.round(Dimensions.get('window').width);
    return (
      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column',justifyContent: 'center',}} behavior="padding" enabled   keyboardVerticalOffset={0}>
      <ScrollView>
      <Image
        source={require('../assets/images/Log.png')}
        style={{ width: screenWidth, marginTop: -120, alignItems: 'center'}}
        resizeMode="contain"
      />
      <View style={{ margin: 20 }}>
        <Input
          placeholder="Adresse e-mail"
          onChangeText={(username) => this.setState({username})}
          autoCapitalize='none'
        />
      </View>
      <View style={{ margin: 20 }}>
        <Input
          placeholder="Mot de passe"
          onChangeText={(password) => this.setState({password})}
          secureTextEntry
        />
      </View>
      <View style={[styles.loader, styles.horizontal]}>
      {this.state.loader}
    </View>
    {this.checkButton(10, "#00ace0", "#ffffff", "CONNEXION", this._signInAsync)}
    {this.checkButton(0,  "#ffffff", "#00ace0", "Créer un compte", () => this.props.navigation.navigate('SignUp'))}
      </ScrollView>
      </KeyboardAvoidingView>
    );
  }

  _signInAsync = async () => {
    var email = this.state.username;
    email = email.toLowerCase();
    if (!Connection.functions.validateEmail(email)) {
      Alert.alert('Erreur: email incorrect', '',
      [{text: 'OK', onPress: () => console.log('OK Pressed')},],
      {cancelable: false})
    } else {
      this.setState({loader: <ActivityIndicator size="large" color="#00ace0" />});
      Connection.functions.login(this.state.username, this.state.password, this.props);
    }
  };
}

export default SignInScreen;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});
