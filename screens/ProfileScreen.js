import React from 'react';
import { ScrollView, StyleSheet, AsyncStorage, Image,
   Text, View, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { Avatar } from 'react-native-elements';
import axios from 'axios';
import blankImg from '../assets/images/blank.png';

const ACCESS_TOKEN = 'access_token';
const decode = require('jwt-decode');
const screenWidth = Math.round(Dimensions.get('window').width);

class ProfileScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      auth: null,
      userId: null,
      pictureId: null,
      username: null,
      vbook: [],
      buy_vbook: [],
      creaTtile: "MES CRÉATIONS",
      buyTitle: "MES ACHATS",
    }
  };

/****************** CLASSIC FUNCTIONS *****************/

/****************** ASYNC FUNCTIONS *****************/

async getVBookPicture(id, title, about, vbookId) {
  await axios.get('https://vbookcreator-dev.herokuapp.com/upload-image/' + id, {headers: {Authorization: this.auth}})
      .then(res => {
        this.setState(prevState => ({
          vbook: [...prevState.vbook, {"img": res.data.uri, "title": title, "about": about, vbookId}]
        }))
      })
      .catch((error) => {
        console.log('Profile-getVBookPicture : ', error);
      })
}

async getBuyVBookPicture(id, title, about, vbookId) {
  await axios.get('https://vbookcreator-dev.herokuapp.com/upload-image/' + id, {headers: {Authorization: this.auth}})
      .then(res => {
        this.setState(prevState => ({
          buy_vbook: [...prevState.buy_vbook, {"img": res.data.uri, "title": title, "about": about, vbookId}]
        }))
      })
      .catch((error) => {
        if (id == "vbook-blank.png") {
          this.setState(prevState => ({
            buy_vbook: [...prevState.buy_vbook, {"img": "blank", "title": title, "about": about, vbookId}]
          }))
        }
        console.log('Profile-getBuyVBookPicture : ', error);
      })
}

async getVBook() {
  await axios.get('https://vbookcreator-dev.herokuapp.com/vbooks/?createdBy=' + this.userId, {headers: {Authorization: this.auth}})
      .then(res => {
        this.setState({
          data: res.data.data,
        });
      })
      .catch((error) => {
        console.log('Profile-getVBook' + error);
      });
      this.state.data.map((y) => {
        if (y.picture == "vbook-blank.png") {
          this.setState(prevState => ({
            vbook: [...prevState.vbook, {"img": "blank", "title": y.title, "about": y.about, vbook_id: y._id}]
          }))
        } else {
          this.getVBookPicture(y.picture, y.title, y.about, y._id);
        }
    });
}

async getBuyVBookPictureId(vbook) {
  await axios.get('https://vbookcreator-dev.herokuapp.com/vbooks/' + vbook.vbook, {headers: {Authorization: this.auth}})
      .then(res => {
        this.getBuyVBookPicture(res.data.picture, res.data.title, res.data.about, vbook.vbook)
      })
      .catch((error) => {
        console.log('Profile-getBuyVBookPictureId' + error);
      });
}

async getBuyVBook() {
  await axios.get('https://vbookcreator-dev.herokuapp.com/purchases/?user=' + this.userId, {headers: {Authorization: this.auth}})
      .then(res => {
        this.setState({
          data: res.data.data,
        });
      })
      .catch((error) => {
        console.log('Profile-getBuyVBook' + error);
      });
    this.state.data.map((y) => {
      this.getBuyVBookPictureId(y);
    });

}

async getUserPicture() {
  await axios.get('https://vbookcreator-dev.herokuapp.com/upload-image/' + this.state.pictureId, {headers: {Authorization: this.auth}})
      .then(res => {
        this.setState({
          pic: res.data.uri,
        });
      })
      .catch((error) => {
        console.log('Profile-getUserPicture : ' + error);
      });
}

async getUserData() {
  await axios.get('https://vbookcreator-dev.herokuapp.com/users/' + this.userId, {headers: {Authorization: this.auth}})
      .then(res => {
        this.setState({
          pictureId: res.data.profilePicture,
          username: res.data.username,
        });
      })
      .catch((error) => {
        console.log('Profile-getUserData' + error);
      });
}

async getToken() {
    try {
      let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
      if (!accessToken) {
        console.log("Token not set");
      } else {
        var userId = decode(accessToken);
        console.log('ID ', userId);
        return accessToken;
      }
    } catch (error) {
      console.log("Profile-getToken : ", error);
    }
  }

async _asyncGet() {
  await this.getVBook();
  await this.getBuyVBook();
  await this.getUserData();
  await this.getUserPicture();
}

async componentDidMount() {
  const token = await this.getToken();
    this.auth = 'Bearer '.concat(token);
    this.userId = decode(token).userId;
    this._asyncGet();
    if (this.state.vbook == []) {
      this.setState({ creaTtile: " " })
    }
    if (this.state.buy_vbook == []) {
      this.setState({ buyTitle: " " })
    }

    this.didFocusListener = this.props.navigation.addListener(
    'didFocus',
    () => {
            console.log('did focus');
            this.setState({ vbook: []});
            this.setState({ buy_vbook: []});
            this._asyncGet();
          },
  );
}

/***************  RENDER **********************/

renderImg = (vbook) => {
  if (vbook.img == "blank") {
    return (
      <Image source={ blankImg }
      style={{height: 100, width: (screenWidth / 3) - 20}} />
    );
  }
  return (
    <Image source={{ uri: `${vbook.img}`}}
      style={{height: 100, width: (screenWidth / 3) - 20}}/>
  );
}

onItemPressed = (title, img, about) => {
    this.props.navigation.navigate('Details', {title: title, img: img, about: about, gotIt: true});
  }


renderItem = vbook => {
  let id = vbook.item.vbook_id
  if (!id) {
    id = vbook.item.vbookId
  }
  return (
    <View style={{borderWidth: 0.5, borderColor: '#d6d7da', backgroundColor: 'white', margin: 10, borderRadius: 12, overflow: 'hidden'}}>
    <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {vbook: vbook.item})}>
          {this.renderImg(vbook.item)}
        </TouchableOpacity>
        <Text
            style={{margin: 10, width: (screenWidth / 3 - 40), fontWeight: 'bold'}}
            numberOfLines={2}>
            {vbook.item.title}
          </Text>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Scene', {vbook_id: id})}>
          <Image source={require("../assets/images/play.png")}
          style={{height: 40, width: 40, marginLeft: (((screenWidth / 3) - 20) / 2) - 20, marginBottom: 10 }}/>
        </TouchableOpacity>
    </View>
  );
}

  render() {
    return (
      <ScrollView>
        <View style={{display: 'flex', alignItems: 'center', marginTop: 20,  borderBottomColor: 'black'}}>
          <Avatar rounded source={{ uri: `${this.state.pic}` }} size="large" />
          <Text style={{textAlign: 'center', margin: 10, fontWeight: 'bold', fontSize: 20}}> {this.state.username} </Text>
        </View>
        <Text style={{ fontWeight: 'bold', color: "#00ace0", fontSize: 20, marginLeft: 10 }}>
          {this.state.creaTtile}
        </Text>
        <FlatList
          data={this.state.vbook}
          keyExtractor={(item, index) => index.toString()}
          renderItem={(item) => this.renderItem(item)}
          numColumns={3}
          style={{ paddingBottom: 20 }}
        />
        <Text style={{ fontWeight: 'bold', color: "#00ace0", fontSize: 20, marginLeft: 10 }}>
          {this.state.buyTitle}
        </Text>
        <FlatList
          data={this.state.buy_vbook}
          keyExtractor={(item, index) => index.toString()}
          renderItem={(item) => this.renderItem(item)}
          numColumns={3}
          style={{ paddingBottom: 20 }}
        />
      </ScrollView>
    );
  }
}

ProfileScreen.navigationOptions = {
  title: 'Profil',
};

export default ProfileScreen
