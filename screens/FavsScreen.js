import React from 'react';
import { ScrollView, StyleSheet, FlatList, AsyncStorage,
   View, Text, Image, Dimensions, TouchableOpacity, ActivityIndicator } from 'react-native';
import axios from 'axios';
import blankImg from '../assets/images/blank.png';

const decode = require('jwt-decode');
const moment = require('moment');
const ACCESS_TOKEN = 'access_token';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class FavsScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: null,
      favs_vbook: [],
      vbook: [],
      auth: null,
      user_id: null,
      data: [],
      favs: [],
      showHeart: false,
      focus: false,
    };
  }

  /****************** CLASSIC FUNCTIONS *****************/

    checkFav(vbook_id) {
      var i = 0;
      this.state.favs.map((y) => {
        if (vbook_id == y.vbook) {
          i = 1;
        }
      });
      if (i == 1) {
        this.showHeart = true;
      } else {
        this.showHeart = false
      }
    }

  /****************** ASYNC FUNCTIONS *****************/

  async deleteFav(vbook) {
    this.state.favs.map((y) => {
      if (vbook.vbook_id == y.vbook) {
        axios.delete('https://vbookcreator-dev.herokuapp.com/favourites/' + y._id, { headers: { Authorization: this.auth } })
        .then(function (response) {
          console.log("delete fav");
        })
        .catch(function (e) {
          console.log(e);
        });
      }
    });
      this.setState({ favs_vbook: []});
    this._asyncGet();
  }

  async getFavs() {
    await axios.get('https://vbookcreator-dev.herokuapp.com/favourites?user=' + this.user_id, { headers: { Authorization: this.auth } })
    .then(res => {
      this.setState({
        favs: res.data.data,
      });
    })
    .catch((error) => {
      console.log('Fav-getFavs : ' + error);
    });
  }

  async getVBookPicture(picture_id, title, about, vbook_id, vbook_date) {
    await axios.get('https://vbookcreator-dev.herokuapp.com/upload-image/' + picture_id, { headers: { Authorization: this.auth } })
    .then(res => {
      this.checkFav(vbook_id);
      if (this.showHeart == true) {
        this.setState(prevState  =>  ({
          favs_vbook: [...prevState.favs_vbook, {"img": res.data.uri, "title": title, "about": about, "vbook_id": vbook_id,
                                      "picture_id": picture_id, "createdAt": vbook_date, "fav": this.showHeart}]
                                    }))
      }
    })
    .catch((error) => {
      this.checkFav(vbook_id);
      if (this.showHeart == true) {
        this.setState(prevState  =>  ({
          favs_vbook: [...prevState.favs_vbook, {"img": "blank", "title": title, "about": about, "vbook_id": vbook_id,
                                      "picture_id": picture_id, "createdAt": vbook_date, "fav": this.showHeart}]
                                    }))
      }
    })
  }

  async getVBookData() {
    await axios.get('https://vbookcreator-dev.herokuapp.com/vbooks?$limit=25', { headers: { Authorization: this.auth } })
    .then(res => { this.setState({ data: res.data.data }) })
    .catch((error) => {
      console.log("Home-getVBookData : ", error);
    })
  }

  async getToken() {
    try {
      let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
      if (!accessToken) {
        console.log("Token not set");
      } else {
        var userId = decode(accessToken);
        this.user_id = userId.userId;
        return accessToken;
      }
    } catch(error) {
      console.log("Home-getToken : ", error);
    }
  }

  async _asyncGet() {
    await this.getFavs();
    await this.getVBookData();
    this.state.data.map((i) => {
      this.getVBookPicture(i.picture, i.title, i.about, i._id, i.createdAt);
    });
  }

  async componentDidMount() {
    const token = await this.getToken();
    const AuthStr = 'Bearer '.concat(token);
    this.auth = AuthStr;
    this._asyncGet();

    this.didFocusListener = this.props.navigation.addListener(
    'didFocus',
    () => {
            console.log('did focus');
            this.setState({ favs_vbook: []});
            this._asyncGet();
          },
  );
  }

  /****************** RENDER FUNCTIONS *****************/

  renderImg = (vbook) => {
    if (vbook.img == "blank") {
      return (
        <Image source={ blankImg }
        style={{  height: screenHeight / 8, width: (screenWidth / 2) - 10 }} />
      );
    }
    return (
      <Image source={{ uri: `${vbook.img}`}}
        style={{  height: screenHeight / 8, width: (screenWidth / 2) - 10 }}/>
    );
  }

  renderFavItem = (item) => {
    return (
          <View style={{borderWidth: 0.5,borderColor: '#d6d7da', backgroundColor: 'white', width: ((screenWidth / 2) - 15), marginLeft: 10, marginTop: 10,
                      borderRadius: 6, overflow: 'hidden' }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {vbook: item.item})}>
            {this.renderImg(item.item)}
          </TouchableOpacity>
          <Text style={{ marginLeft: 5, marginTop: 10, fontWeight: 'bold' }}> {item.item.title} </Text>
          <Text style={{ marginLeft: 5 }}> Auteur </Text>
          <Text style={{ margin: 10 }}> 4€ </Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {vbook: item.item})}
              style={{ backgroundColor: "#00ace0", justifyContent: 'center', alignItems: 'center', width: ((screenWidth / 2) - 15) / 2 }}>
              <Image source={require('../assets/images/view.png')}
                style={{width: 30, height: 40}}
                resizeMode="contain"/>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.deleteFav(item.item)}
              style={{justifyContent: 'center', alignItems: 'center', width: ((screenWidth / 2) - 15) / 2}}>
              <Image style={{width: 30, height: 30}} source={require('../assets/images/garbage.png')}/>
            </TouchableOpacity>
          </View>
        </View>
    )
  }

  render() {
    return (
      <ScrollView style={styles.container}>
      <View style={[styles.loader, styles.horizontal]}>
      {this.state.loader}
    </View>
        <FlatList
          data={this.state.favs_vbook}
          keyExtractor={(item, index) => index.toString()}
          renderItem={(item) => this.renderFavItem(item)}
          numColumns={2}
          style={{ paddingBottom: 20 }}
        />
      </ScrollView>
    );
  }
}

FavsScreen.navigationOptions = {
  title: 'Favoris',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});

export default FavsScreen
