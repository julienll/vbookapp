import React from 'react';
import { View, Text, AsyncStorage } from 'react-native';
import axios from 'axios';
import {WebView} from 'react-native-webview';
import { ScreenOrientation } from 'expo';

const ACCESS_TOKEN = 'access_token';
const decode = require('jwt-decode');

class SceneScreen extends React.Component {
  constructor(props) {
    super(props);

    const { navigation } = this.props;
    const vbook_id = navigation.getParam('vbook_id');

    this.state = {
      vbook_id: vbook_id,
    };
  }

  static navigationOptions = {
    title: 'SCENE',
  };

  async getToken() {
      try {
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        if (!accessToken) {
          console.log("Token not set");
        } else {
          var userId = decode(accessToken);
          console.log('ID ', userId);
          return accessToken;
        }
      } catch (error) {
        console.log("Profile-getToken : ", error);
      }
    }

  async componentDidMount() {
    const token = await this.getToken();
    this.setState({ auth: token})
    ScreenOrientation.lockAsync(ScreenOrientation.Orientation.LANDSCAPE);
  }

  componentWillUnmount() {
      ScreenOrientation.lockAsync(ScreenOrientation.Orientation.PORTRAIT);
    }


  switchToLandscape() {
     ScreenOrientation.lockAsync(ScreenOrientation.Orientation.LANDSCAPE);
    }


  render() {
    this.switchToLandscape();
    console.log(this.state.vbook_id);
    if (this.state.vbook_id) {
    const uri = `https://vbookcreator-renderer-dev.herokuapp.com/?token=${this.state.auth}&vbookId=${this.state.vbook_id}`;
    console.log(uri);
    return (
            <WebView source={{ uri: uri }}/>
    );
  }
}
}

export default SceneScreen;
