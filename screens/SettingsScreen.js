import React from 'react';
import { ScrollView, StyleSheet, AsyncStorage, View,
   Text, Button, Image, Dimensions,
   TouchableOpacity, Alert, Platform, KeyboardAvoidingView } from 'react-native';
import { Avatar, Input } from 'react-native-elements';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Connection from '../utils/Connection';
import ActionSheet from 'react-native-actionsheet';
import axios from 'axios';

const ACCESS_TOKEN = 'access_token';
const decode = require('jwt-decode');

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Paramètres',
  };

  constructor(props) {
    super(props);
    this.state = {
      auth: null,
      userId: null,
      username: null,
    }
  }

  /****************** CLASSIC FUNCTIONS *****************/

  onUserNameInputChanged = (text) => {
          this.setState({ new_username: text });
        };

        onEmailInputChanged = (text) => {
      this.setState({ new_email: text });
    };

    onNewPasswordInputChanged = (text) => {
      this.setState({ new_password: text });
    };

    onConfirmPasswordInputChanged = (text) => {
      this.setState({ new_confirm_password: text });
    };

    showActionSheet = () => {
      this.ActionSheet.show();
  };

  /****************** ASYNC FUNCTIONS *****************/

  async postPic() {
    if (!this.state.result.cancelled) {
        let uri = this.state.result.uri;

        let filename = uri.split('/').pop();
        let data = new FormData();
        console.log(uri, filename);
        data.append('image', {
          uri: uri,
          name: filename,
          type: 'image/jpg'
        });
        await fetch('https://vbookcreator-dev.herokuapp.com/upload-image?linkTo=user', {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': this.auth
          },
          method: 'POST',
          body: data
        })
        .then((responseJson) => {
          console.log('IMAGE SUCCESS', JSON.stringify(responseJson));
        })/*
        .catch((error) =>{
          console.error('IMAGE ERROR', JSON.stringify(error));
        });*/
        this.setState({ pic: null})
        this._asyncGet();
      }
    }

  async pickGalery() {
    await this.getPermissionAsyncGalery();
    let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
      });
      if (!result.cancelled) {
        this.setState({ result: result });
      }
      this.postPic();
}

  async pickCamera() {
    await this.getPermissionAsyncCamera();
    let result = await ImagePicker.launchCameraAsync();
    this.setState({ result: result });
    this.postPic();
  }

  _pickImage = async (choice) => {
    if (choice === "Appareil photo") {
      this.pickCamera();
    }
    else if (choice === "Gallerie") {
      this.pickGalery();
    }
  }

  async disconnect() {
    try {
      let accessToken = await AsyncStorage.removeItem(ACCESS_TOKEN);
      if (!accessToken) {
        console.log("Token removed");
        this.props.navigation.navigate('Auth');
      } else {
        console.log("Token not removed");
      }
    } catch(error) {
      console.log("ERROR", error);
    }
  }


  async getUserPicture() {
    await axios.get('https://vbookcreator-dev.herokuapp.com/upload-image/' + this.state.pictureId, {headers: {Authorization: this.auth}})
        .then(res => {
          this.setState({
            pic: res.data.uri,
          });
        })
        .catch((error) => {
          console.log('Profile-getUserPicture : ' + error);
        });
  }

  async getUserData() {
    await axios.get('https://vbookcreator-dev.herokuapp.com/users/' + this.userId, {headers: {Authorization: this.auth}})
        .then(res => {
          this.setState({
            pictureId: res.data.profilePicture,
            username: res.data.username,
            email: res.data.email,
          });
          console.log(res.data);
        })
        .catch((error) => {
          console.log('Profile-getUserData' + error);
        });
  }

  async getToken() {
      try {
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        if (!accessToken) {
          console.log("Token not set");
        } else {
          var userId = decode(accessToken);
          console.log('ID ', userId);
          return accessToken;
        }
      } catch (error) {
        console.log("Profile-getToken : ", error);
      }
    }

    async getPermissionAsyncGalery() {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Désolé, nous avons besoin de votre autorisation pour accéder à vos photos');
      }
    }
  }

  async getPermissionAsyncCamera() {
  if (Constants.platform.ios) {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    if (status !== 'granted') {
      alert('Désolé, nous avons besoin de votre autorisation pour accéder à vos photos');
    }
  }
}

  async saveChanges() {
    var data;

     if ((this.state.new_password != null && this.state.new_confirm_password == null) || (this.state.new_password == null && this.state.new_confirm_password != null)) {
       Alert.alert(
        'Erreur, les 2 champs doivent etre remplis',
         '',
         [
           {text: 'OK'},
         ],
         { cancelable: false }
       );
       return;
     }
     console.log(this.state.new_username);
     if (this.state.new_username == '' || (this.state.new_email == '' && !Connection.functions.validateEmail(this.state.new_email))) {
       Alert.alert(
        'Erreur, veuillez choisir un username et un mail valide',
         '',
         [
           {text: 'OK'},
         ],
         { cancelable: false }
       );
       return;
     }

     if (this.state.new_password != this.state.new_confirm_password) {
       Alert.alert(
        'Erreur: les mots de passe doivent correspondre',
         '',
         [
           {text: 'OK'},
         ],
         { cancelable: false }
       );
       return;
     }

     if (this.state.new_password == null) {
       data = { email: this.state.new_email,
       username: this.state.new_username };
     } else {
       data = { email: this.state.new_email,
       username: this.state.new_username,
       password: this.state.new_password };
     }

     await axios.patch('https://vbookcreator-dev.herokuapp.com/users/' + this.userId, data, {
       headers: { Authorization: this.auth }
     })
     .then(response => {
       Alert.alert(
        'Sauvegarde réussie !',
         '',
         [
           {text: 'OK'},
         ],
         { cancelable: false }
       )
     }).catch(err => {
       Alert.alert(
         'Erreur',
         '',
          [
           {text: 'OK'},
          ],
          { cancelable: false }
        )
       console.log(err);
     });
     this._asyncGet();
  }

  async _asyncGet() {
    await this.getUserData();
    await this.getUserPicture();
  }

  async componentDidMount() {
    const token = await this.getToken();
    this.auth = 'Bearer '.concat(token);
    this.userId = decode(token).userId;
    this._asyncGet();
  }

  /***************  RENDER **********************/

  checkButton = (padd, background, text, title, press) => {
    if (Platform.OS == "ios") {
    return (
      <View style={{backgroundColor: background, margin: 20, borderRadius: 6, padding: padd }}>
      <Button
        title={title}
        onPress={press}
        color={text}
      />
      </View>)
    } else {
      if (padd == 0) {
        return (
          <View style={{borderRadius: 6, padding: padd }}>
          <Button
          title={title}
          onPress={press}
          color={text}
          />
          </View>)
      } else {
      return (
        <View style={{borderRadius: 6, padding: padd }}>
        <Button
        title={title}
        onPress={press}
        color={background}
        />
        </View>)}
    }
  }

  render() {
    var optionArray = [
      'Appareil photo',
      'Gallerie',
      'Cancel',
    ];
    return (
      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column',justifyContent: 'center',}} behavior="padding" enabled   keyboardVerticalOffset={0}>
      <ScrollView>
      <View style={{display: 'flex', alignItems: 'center', marginTop: 20,  borderBottomColor: 'black', marginBottom: 30}}>
        <Avatar rounded source={{ uri: `${this.state.pic}` }} size="xlarge" />
      </View>
      <TouchableOpacity onPress={() => this.showActionSheet()} style={{position: 'absolute'}}>
      <ActionSheet ref={o => (this.ActionSheet = o)} title={'Selectionnez la source de la photo'} options={optionArray} cancelButtonIndex={2} destructiveButtonIndex={2} onPress={index => {this._pickImage(optionArray[index]);}}/>
      <Image style={{width: 60, height: 60, marginTop: 130, marginLeft: (screenWidth / 2 + 20)}} source={require('../assets/images/pencil.png')}>
      </Image>
      </TouchableOpacity>
      <View style={{ margin: 20}}>
      <Text style={{fontWeight: 'bold', marginLeft: 10}}> Votre nom d'utilisateur actuel est : {this.state.username} </Text>
        <Input
          placeholder={this.state.username}
          autoCapitalize='none'
          onChangeText={this.onUserNameInputChanged}
        />
        </View>
        <View style={{ margin: 20 }}>
        <Text style={{fontWeight: 'bold', marginLeft: 10}}> Votre adresse mail actuelle est : {this.state.email} </Text>
          <Input
            placeholder={this.state.email}
            autoCapitalize='none'
             onChangeText={this.onEmailInputChanged}
          />

      </View>
      <View style={{ margin: 20 }}>
        <Input
          placeholder="Mot de passe"
          secureTextEntry
          onChangeText={this.onNewPasswordInputChanged}
        />
      </View>
      <View style={{ margin: 20 }}>
        <Input
          placeholder="Confirmation Mot de passe"
          secureTextEntry
          onChangeText={this.onConfirmPasswordInputChanged}
        />
      </View>
      {this.checkButton(10, "#00ace0", "#ffffff", "ENREGISTRER", () => this.saveChanges())}
      {this.checkButton(10, "#e00000", "#ffffff", "DÉCONNEXION", () => this.disconnect())}
      </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default SettingsScreen;
