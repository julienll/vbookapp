import React from 'react';
import { View, Button, AsyncStorage, ScrollView,
  Image, Dimensions, Alert, StyleSheet,
  ActivityIndicator, Platform, KeyboardAvoidingView } from 'react-native';
import { Input } from 'react-native-elements';
import Connection from '../utils/Connection';

class SignUpScreen extends React.Component {
  static navigationOptions = {
    title: 'Inscription',
  };

  constructor(props) {
    super(props);
    this.state = {
      mail: '',
      username: '',
      password: '',
      confirm_password: '',
      loader: null,
    };
  }

  checkButton = (padd, background, text, title, press) => {
    if (Platform.OS == "ios") {
    return (
      <View style={{backgroundColor: background, margin: 20, borderRadius: 6, padding: padd }}>
      <Button
        title={title}
        onPress={press}
        color={text}
      />
      </View>)
    } else {
      if (padd == 0) {
        return (
          <View style={{borderRadius: 6, padding: padd }}>
          <Button
          title={title}
          onPress={press}
          color={text}
          />
          </View>)
      } else {
      return (
        <View style={{borderRadius: 6, padding: padd }}>
        <Button
        title={title}
        onPress={press}
        color={background}
        />
        </View>)}
    }
  }

  render() {
    const screenWidth = Math.round(Dimensions.get('window').width);
    return (
      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column',justifyContent: 'center',}} behavior="padding" enabled   keyboardVerticalOffset={0}>
      <ScrollView>
      <Image
        source={require('../assets/images/Log.png')}
        style={{ width: screenWidth, marginTop: -120, alignItems: 'center'}}
        resizeMode="contain"
      />
      <View style={{ marginLeft: 20, marginRight: 20, marginBottom: 20, marginTop: -40 }}>
        <Input
          placeholder="Adresse e-mail"
          onChangeText={(mail) => this.setState({mail})}
          autoCapitalize='none'
          />
      </View>
      <View style={{ margin: 20 }}>
        <Input
          placeholder="Nom d'utilisateur"
          onChangeText={(username) => this.setState({username})}
          autoCapitalize='none'
        />
      </View>
      <View style={{ margin: 20 }}>
        <Input
          placeholder="Mot de passe"
          onChangeText={(password) => this.setState({password})}
          secureTextEntry
        />
      </View>
      <View style={{ margin: 20 }}>
        <Input
          placeholder="Confirmation mot de passe"
          onChangeText={(confirm_password) => this.setState({confirm_password})}
          secureTextEntry
        />
      </View>
      <View style={[styles.loader, styles.horizontal]}>
      {this.state.loader}
    </View>
    {this.checkButton(10, "#00ace0", "#ffffff", "INSCRIPTION", this._signUpAsync)}
      </ScrollView>
      </KeyboardAvoidingView>
    );
  }

  _signUpAsync = async () => {
    var email = this.state.mail;
    email = email.toLowerCase();
    if (this.state.mail == '' || this.state.username == '' || this.state.password == '' || this.state.confirm_password == '') {
      Alert.alert('Merci de remplir tous les champs', '',
      [{text: 'OK', onPress: () => console.log('OK Pressed')},],
      {cancelable: false})
    } else if (this.state.password != this.state.confirm_password) {
      Alert.alert('La confirmation du mot de passe doit être identique au mot de passe', '',
      [{text: 'OK', onPress: () => console.log('OK Pressed')},],
      {cancelable: false})
    } else if (!Connection.functions.validateEmail(email)) {
      Alert.alert('Erreur: email incorrect', '',
      [{text: 'OK', onPress: () => console.log('OK Pressed')},],
      {cancelable: false})
    } else {
      this.setState({loader: <ActivityIndicator size="large" color="#00ace0" />});
      Connection.functions.signup(this.state.mail, this.state.username, this.state.password, this.props)
    }
  };
}

export default SignUpScreen;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});
