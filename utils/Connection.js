import axios from 'axios';
import NavigationType from '../navigators/propTypes';
import { Alert, AsyncStorage } from 'react-native';

class Connection {}

const ACCESS_TOKEN = 'access_token';

Connection.getToken = async function() {
  try {
    let token = await AsyncStorage.getItem(ACCESS_TOKEN);
    console.log("token is : " + token);
  } catch(error) {
    console.log("something went wrong");
  }
}

Connection.storeToken = async function(accessToken) {
  try {
    await AsyncStorage.setItem(ACCESS_TOKEN, accessToken);
    Connection.getToken();
  } catch(error) {
    console.log(error);
  }
}

Connection.validateEmail = function(email) {
  const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regexp.test(email);
}


Connection.login = function(username, password, props) {
  navigationOptions = {
    header: null,
  };
  propTypes = {
    navigation: NavigationType.isRequired,
  };

  axios.post('https://vbookcreator-dev.herokuapp.com/authentication', {email: username, password: password, strategy: 'local'})
  .then(response => {
    Connection.storeToken(response['data']['accessToken']);
    props.navigation.navigate('Main')
  }).catch(err => {
    Alert.alert(
      'Mauvais identifiants',
      '',
       [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
       ],
       { cancelable: false }
     )
    console.log(err);
    return false;
  });
  return true;
}

Connection.signup = function(mail, username, password, props) {
  axios.post('https://vbookcreator-dev.herokuapp.com/users', {email: mail, password: password, username: username, strategy: 'local'})
  .then(response => {
    this.login(mail, password, props);
    }).catch(err => {
    Alert.alert(
      'Erreur lors de la création du compte',
      '',
       [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
       ],
       { cancelable: false }
     )
    console.log(err);
  });
}

module.exports = {
  functions: Connection
};
